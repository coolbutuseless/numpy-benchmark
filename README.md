Micro-benchmarks for some numpy functions
=========================================

* Benchmarking to compare numpy implementations
* Micro-benchmarks using `timeit`
* Currently, most benchmarks are for *in-place* operations as I'm wanting to test algorithm time, and not conflate it with memory allocation time.


Usage
-----
* `./benchmark.py [numpy library name]`  (defaults to **numpy**)
  - writes timings to a file of tab-separated values (TSV)
* Graph with `benchmark.Rmd` in [Rstudio](rstudio.com)
  - just set the names of the TSV files to compare.


Output
------
* See the benchmark from a [4core i7 OSX machine](http://rpubs.com/mikefc/60129)
