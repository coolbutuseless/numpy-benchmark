#!/usr/bin/env python
from __future__ import print_function, division

import sys
import csv
import timeit
import datetime

if len(sys.argv) > 2:
    print("Usage:\n\tbenchmark.py [numpy name]\n")

arraylib = "numpy"
if len(sys.argv) == 2:
    # Optional: pass in the name of a numpy compatible library 
    arraylib = sys.argv[1]

datestring = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
Ns = [2**x for x in range(2, 8)]  # What vector/matrix sizes to use
target_time = 5      # The total time for each benchmark in seconds (approx)
repeat = 1

# Initial number of iterations to work out timing
Niters_init = 400

runtime = "pypy" if "__pypy__" in sys.builtin_module_names else "CPython"

csv_filename = datestring + '-' + arraylib + '-' + runtime + '.tsv'

setup_base = """
import {arraylib} as np

a3=np.random.rand(3)
b3=np.random.rand(3)

N = {N}

a=np.random.rand({N})
b=np.random.rand({N})
out=np.random.rand({N})

M    = np.random.rand({N} * {N}).reshape(({N}, {N}))
P    = np.random.rand({N} * {N}).reshape(({N}, {N}))
Mout = np.random.rand({N} * {N}).reshape(({N}, {N}))
Mout_ravel = Mout.ravel()
"""


benchmarks = [
        # array creation
        "np.zeros(N)", "np.zeros((N, N))",
        "np.ones(N)" , "np.ones((N, N))",
        "a.copy()"   , "M.copy()", 
        "np.random.rand(N)", "np.random.rand(N*N)",

        # vector/scalar arithmetic
        'a += 3.1',
        'a *= 2.3',
        'a /= 2.2',
        'a **= 2.1',

        'M += 3.1',
        'M *= 2.3',
        'M /= 2.2',
        'M **= 2.1',

        # Vector/Vector arithmetic
        "a*=b", "a+=b", "a/=b", "a-=b",
        "M*=P", "M+=P", "M/=P", "M-=P",

        # dot product
        "a.dot(b)", "M.dot(a, out=out)",

        # cross product
        "np.cross(a3, b3)",

        # linalg
        "np.linalg.norm(a)", 

        # np funcs
        "np.minimum(a, b, out=out)",
        "np.maximum(a, b, out=out)",
        "np.minimum(M, P, out=Mout)",
        "np.maximum(M, P, out=Mout)",

        # one_func
        'np.sqrt(a, out=out)', 'np.sqrt(M, out=Mout)',
         'np.sin(a, out=out)',  'np.sin(M, out=Mout)',
         'np.log(a, out=out)',  'np.log(M, out=Mout)',

        # reduction
        'M.max()'   ,    'M.max(axis=0, out=out)',    'M.max(axis=1, out=out)',
        "M.sum()"   ,    "M.sum(axis=0, out=out)",    'M.sum(axis=1, out=out)',
        'M.var()'   ,    'M.var(axis=0, out=out)',    'M.var(axis=1, out=out)',
        'M.std()'   ,    'M.std(axis=0, out=out)',    'M.std(axis=1, out=out)',
        "M.prod()"  ,   "M.prod(axis=0, out=out)",   'M.prod(axis=1, out=out)',
        'M.argmax()', 'M.argmax(axis=0, out=out)', 'M.argmax(axis=1, out=out)',

        # Accumulations. no out arg available. Matrices
                                     'M.sort(axis=0)',     'M.sort(axis=1)',    'a.sort()',
        'np.sort(M, axis=None)', 'np.sort(M, axis=0)', 'np.sort(M, axis=1)',   'np.sort(a)',
        'M.argsort()',            'M.argsort(axis=0)',  'M.argsort(axis=1)', #'a.argsort()',


        # Accumulations. 'out' arguments
         'a.cumsum(out=out)',  'M.cumsum(out=Mout_ravel)', 'M.cumsum(axis=0, out=Mout)',  'M.cumsum(axis=1, out=Mout)',
        'a.cumprod(out=out)', 'M.cumprod(out=Mout_ravel)','M.cumprod(axis=0, out=Mout)', 'M.cumprod(axis=1, out=Mout)',

]

def microbench(arraylib, N, stmt):
    setup = setup_base.format(arraylib=arraylib, N=N)

    Niters = Niters_init
    # Give an extra warmup cycle for pypy as the JIT makes timing estimates problematic
    warmups = 2 if runtime == 'pypy' else 1
    for _ in range(warmups):
        total_time = timeit.timeit(stmt=stmt, setup=setup , number=Niters)
        Niters = int(target_time / total_time * Niters)

    # Best of 'repeat' timings
    total_time = min(timeit.repeat(stmt=stmt, setup=setup, repeat=repeat, number=Niters))

    return Niters, total_time, total_time/Niters * 1e6

fieldnames = ["date", "runtime", "Niters", "N", "benchmark", "ttime", "time"]
with open(csv_filename, 'w') as csvfile:
    csvfile.write("\t".join(fieldnames) + "\n")
    for stmt in benchmarks:
        for N in Ns:
            Niters, total_time, time = 'NA', 'NA', 'NA'
            try:
                Niters, total_time, time = microbench(arraylib, N, stmt)
            except TypeError:
                # Cpython numpy.argsort() doesn't accept 'out' arrays of dtype=f8
                # and I'm only testing f8 arrays
                assert runtime=='CPython' and 'arg' in stmt
                print("arg* functions with out arrays expected to fail in CPython_numpy:", stmt)
            except IndexError:
                assert runtime=='pypy' and arraylib=='numpy' and stmt.startswith('np.cross')
                print("np.cross() not yet working on pypy_numpy")
            except NotImplementedError:
                assert runtime=='pypy' and arraylib=='numpy' and stmt.startswith('M.argmax(axis')
                print("pypy_numpy: does not accept axis argument to argmax")

            # Create output line
            outline = "\t".join(str(x) for x in (datestring, runtime+'_'+arraylib, Niters, N, stmt, total_time, time))
            print(outline)
            csvfile.write(outline + "\n")
            sys.stdout.flush()

